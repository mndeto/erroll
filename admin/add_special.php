<?php
ini_set("display_errors", 0);
ini_set("error_reporting", 0);

include '../connection.php';

if($_SESSION['member']=="Admin"){

include 'header.php';
include 'menu.php';
 ?>
<div id="page-wrapper">
  <div id="page-inner">
  <div class="row">
          <div class="col-md-12">
              <h1 class="page-header">
                  Add New Offer
              </h1>
          </div>
      </div>
       <!-- /. ROW  -->

  <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <?php if(isset($_GET['success'])){?>
              <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success ! </strong>Offer Added Successfully.
              </div>
              <?php } ?>
              <?php if(isset($_GET['error'])){?>
              <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error ! </strong>Sorry, there was an Error. Please try again
              </div>
              <?php } ?>
        <form action="new_offer.php" method="post">
          <div class="form-group">
            <label>Name of Offer</label>
            <input type="text" class="form-control" name="name" required=""><br>
          </div>
          <div class="form-group">
            <label>Price of Offer</label>
            <input type="number" class="form-control" name="price" required=""><br>
          </div>
          <input class="btn btn-success" type="submit" value="Submit">
        </form>
      </div>
  </div>


<?php include 'footer.php'; }else{
  header("location:../login.php");
} ?>
