﻿<?php
ini_set("display_errors", 0);
ini_set("error_reporting", 0);

include '../connection.php';

if($_SESSION['member']=="Admin"){

include 'header.php';
include 'menu.php';
 ?>
 <div id="page-wrapper">
   <div id="page-inner">
   <div class="row">
           <div class="col-md-12">
               <h1 class="page-header">
                   All Special Offers
               </h1>
           </div>
       </div>
        <!-- /. ROW  -->

   <div class="row">
       <div class="col-md-12">
         <a class="btn btn-success" href="add_special.php"><i class="fa fa-plus"></i> Add New Offer </a><br><br>
           <!-- Advanced Tables -->
           <div class="panel panel-default">
               <div class="panel-heading">
                    All Special Offers
               </div>
               <div class="panel-body">
                   <div class="table-responsive">
                     <?php if(isset($_GET['activate'])){?>
                     <div class="alert alert-success alert-dismissable">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <strong>Success ! </strong>Offer Activated
                     </div>
                     <?php } ?>
                     <?php if(isset($_GET['delete'])){?>
                     <div class="alert alert-success alert-dismissable">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <strong>Success ! </strong>Offer Successfully Deleted
                     </div>
                     <?php } ?>
                     <?php if(isset($_GET['deactivate'])){?>
                     <div class="alert alert-success alert-dismissable">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <strong>Success ! </strong>Offer Dectivated
                     </div>
                     <?php } ?>
                     <?php if(isset($_GET['error'])){?>
                     <div class="alert alert-danger alert-dismissable">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       <strong>Error ! </strong>Sorry, there was an Error. Please try again
                     </div>
                     <?php } ?>
                       <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                           <thead>
                               <tr>
                                   <th>Name</th>
                                   <th>Price</th>
                                   <th>Date</th>
                                   <th>Status</th>
                                   <th>Activate</th>
                                   <th>Deactivate</th>
                                   <th>Delete</th>
                               </tr>
                           </thead>
                           <tbody>
                             <?php
                             $sql_all="select * from special";
                             $result_all=mysqli_query($connection,$sql_all);
                             if($result_all){
                               while($row=$result_all->fetch_assoc()){
                                 $id=$row['id'];
                                 $name=$row['name'];
                                 $price=$row['price'];
                                 $status=$row['status'];
                                 $date=$row['date'];

                                 echo '<tr class="odd gradeX">
                                     <td>'.$name.'</td>
                                     <td>'.$price.'</td>
                                     <td>'.$date.'</td>
                                     <td>'.$status.'</td>
                                     <td><a style="color:green" href="activate.php?id='.$id.'"><i class="fa fa-check"></i> Activate</a></td>
                                     <td><a style="color:red" href="deactivate.php?id='.$id.'"><i class="fa fa-times"></i> Deactivate</a></td>
                                     <td><a style="color:red" href="delete.php?id='.$id.'"><i class="fa fa-trash-o"></i></a></td>
                                 </tr>';
                               }
                             }
                              ?>

                           </tbody>
                       </table>
                   </div>

               </div>
           </div>
           <!--End Advanced Tables -->
       </div>
   </div>
<?php include 'footer.php'; }else{
  header("location:../index.php");
} ?>
