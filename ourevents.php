<?php
include 'header.php';
include 'menu.php';

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://www.lord-erroll.com/blog/wp-json/wp/v2/posts",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_POSTFIELDS => "",
	CURLOPT_HTTPHEADER => array(
		"Postman-Token: d58b5d2e-4ce1-47e6-a356-c05322cbd312",
		"cache-control: no-cache"
	),
));



$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    $res = json_decode($response, true);
//echo $res;
    ?>
	<style>
		.event_img{
			height:50em;
			width:100%;
		}
		@media only screen and (max-width: 320px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		@media only screen and (max-width: 480px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		@media only screen and (max-width: 520px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		@media only screen and (max-width: 600px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		@media only screen and (max-width: 720px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		@media only screen and (max-width: 1024px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		@media only screen and (max-width: 1080px) {
			.event_img{
			width:100%;
			height:auto;
			}
		}
		.my_tittle{
			font-size: 3em;
			letter-spacing: 2px;
			text-align: center;
			margin-bottom: 0.5em;
		}
	</style>
    <div class="container" style="margin-bottom: 2em;">
    <h3 class="tittle" style="padding-top:1em;padding-bottom:2em"><span style="color: #dfac18">UPCOMING EVENTS</span></h3>
    <?php

    foreach ($res as $item) {

        foreach ($item['categories'] as $cat) {
            if ($cat == 2) {
                $curl2 = curl_init();

                curl_setopt_array($curl2, array(
                    CURLOPT_URL => "https://www.lord-erroll.com/blog/wp-json/wp/v2/media/" . $item['featured_media'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 3fb64d1f-65d4-ab33-6dfc-5d66c5115149"
                    ),
                ));

                $response2 = curl_exec($curl2);
                $err2 = curl_error($curl2);

                curl_close($curl2);

                $res2 = json_decode($response2, true); ?>

				<div style="margin-bottom:2em;height:auto" class="text-center col-md-6">
                    <a href="<?php echo $item['link']; ?>" target="_blank" >
					<div class="event_img">
                        <img class="erroll-img" src="<?php echo $res2['guid']['rendered']; ?>" alt="" style="max-height:100%;max-width:100%"/>
					</div>
					</a>
					<a href="<?php echo $item['link']; ?>" target="_blank" class="erroll-link2">
                    <h2 style="margin-top:1em;margin-bottom:3em;font-size:23px"><?php echo $item['title']['rendered']; ?></h2>
					</a>
                </div>

            <?php }}} ?>

	<div class="col-md-12">
	<h3 class="my_tittle" style="padding-top:2em;padding-bottom:1em;color: #dfac18">PAST EVENTS</h3>
	</div>
    <?php


    foreach ($res as $item) {
	    foreach ($item['categories'] as $cat) {
		    if ($cat == 3) {

			    $curl3 = curl_init();

			    curl_setopt_array($curl3, array(
				    CURLOPT_URL => "https://www.lord-erroll.com/blog/wp-json/wp/v2/media/" . $item['featured_media'],
				    CURLOPT_RETURNTRANSFER => true,
				    CURLOPT_ENCODING => "",
				    CURLOPT_MAXREDIRS => 10,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				    CURLOPT_CUSTOMREQUEST => "GET",
				    CURLOPT_HTTPHEADER => array(
					    "cache-control: no-cache",
					    "postman-token: 3fb64d1f-65d4-ab33-6dfc-5d66c5115149"
				    ),
			    ));


			    $response3 = curl_exec($curl3);
			    $err2 = curl_error($curl3);

			    curl_close($curl3);

			    $res3 = json_decode($response3, true);

			    ?>

                <div style="margin-bottom:2em;height:auto" class="text-center col-md-6">
                    <a href="<?php echo $item['link']; ?>" target="_blank">
					<div  class="event_img">
                        <img class="erroll-img" src="<?php echo $res3['guid']['rendered']; ?>" alt="" style="max-height:100%;max-width:100%"/>
					</div>
					</a>
					<a href="<?php echo $item['link']; ?>" target="_blank" class="erroll-link2">
                    <h2 style="margin-top:1em;margin-bottom:3em;font-size:23px"><?php echo $item['title']['rendered']; ?></h2>
					</a>
                </div>



            <?php }}} ?>
<?php } ?>

    </div>
        <?php
include 'footer.php'; ?>
