<?php
include 'header.php';
include 'menu.php';
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://www.lord-erroll.com/blog/wp-json/wp/v2/posts",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_POSTFIELDS => "",
	CURLOPT_HTTPHEADER => array(
		"Postman-Token: d58b5d2e-4ce1-47e6-a356-c05322cbd312",
		"cache-control: no-cache"
	),
));



$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$res = json_decode( $response, true );
}
//echo $res;
?>
<style>
    .event_img{
        height:25em;
        width:100%;
    }
    @media only screen and (max-width: 320px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    @media only screen and (max-width: 480px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    @media only screen and (max-width: 520px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    @media only screen and (max-width: 600px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    @media only screen and (max-width: 720px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    @media only screen and (max-width: 1024px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    @media only screen and (max-width: 1080px) {
        .event_img{
            width:100%;
            height:auto;
        }
    }
    .my_tittle{
        font-size: 3em;
        letter-spacing: 2px;
        text-align: center;
        margin-bottom: 0.5em;
    }
</style>

<div class="container" style="border-top: 1px solid #b4b4b4;padding-top:3em">
        <h3 class="tittle" style="padding-top:1em;padding-bottom:2em"><span style="color: #dfac18">GALLERIES</span></h3>
	<?php

	foreach ($res as $item) {

		foreach ($item['categories'] as $cat) {
			if ($cat == 73) {
				$curl2 = curl_init();

				curl_setopt_array($curl2, array(
					CURLOPT_URL => "https://www.lord-erroll.com/blog/wp-json/wp/v2/media/" . $item['featured_media'],
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",
						"postman-token: 3fb64d1f-65d4-ab33-6dfc-5d66c5115149"
					),
				));

				$response2 = curl_exec($curl2);
				$err2 = curl_error($curl2);

				curl_close($curl2);

				$res2 = json_decode($response2, true);

				?>

                <div style="margin-bottom:2em;height:auto" class="text-center col-md-6">
                    <a href="<?php echo $item['link']; ?>" target="_blank" >
                        <div class="event_img">
                            <img class="erroll-img" src="<?php echo $res2['guid']['rendered']; ?>" alt="" style="max-height:100%;max-width:100%"/>
                        </div>
                    </a>
                    <a href="<?php echo $item['link']; ?>" target="_blank" class="erroll-link2">
                        <h2 style="margin-top:1em;margin-bottom:3em;font-size:23px"><?php echo $item['title']['rendered']; ?></h2>
                    </a>
                </div>

			<?php }}} ?>
</div>
<div class="container" style="border-top: 1px solid #b4b4b4;padding-top:3em">
  <h3 class="tittle" style="color: #dfac18">IMAGES</h3>
    <div class="portfolio-agileinfo" id="portfolio" style="margin-bottom: 3em; ">
        <div class="container">
            <div class="wthree_head_section">
<!--                <h3 class="agile_tittle"><i class="fa fa-bullhorn" aria-hidden="true"></i> <span>Our</span>Photo Gallery</h3>-->
            </div>
        </div>
        <div class="agile_wthree_inner_grids">
            <div class="agile_port_w3ls_info">
                <!--                    First Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l12.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l12.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 portfolio-grid_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p4.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p4.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p5.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p5.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--Second Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grid_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p2.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p2.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p1.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p1.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l11.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l11.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--Third Row-->
                <div class="portfolio-grids_main">
                <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                    <a href="images/optimized/l3.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                        <img src="images/optimized/l3.jpg" class="img-responsive" alt="w3ls" />
                        <div class="b-wrapper two">
                            <i class="fa fa-magic" aria-hidden="true"></i>

                        </div>
                    </a>
                </div>
                <div class="col-md-6 portfolio-grid_left">
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/p6.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/p6.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/p7.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/p7.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                </div>
                    <div class="clearfix"> </div>
                </div>

                <!--4th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grids">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p8.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p8.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p12.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p12.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-grid_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p10.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p10.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p11.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p11.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--5th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l4.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l4.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l5.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l5.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--6th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l6.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l6.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 portfolio-grid_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p17.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p17.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p14.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p14.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--7th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grid_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p15.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p15.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p13.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p13.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l7.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l7.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--8th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l1.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l1.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l9.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l9.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <!--8th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l10.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l10.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l2.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l2.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>

                    <div class="clearfix"> </div>
                </div>

                <!--9th Row-->
                <div class="portfolio-grids_main">
                    <div class="col-md-6 portfolio-grid_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p16.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p16.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/optimized/p18.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/optimized/p18.jpg" class="img-responsive" alt="w3ls" />
                                <div class="b-wrapper">
                                    <i class="fa fa-magic" aria-hidden="true"></i>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                        <a href="images/optimized/l8.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                            <img src="images/optimized/l8.jpg" class="img-responsive" alt="w3ls" />
                            <div class="b-wrapper two">
                                <i class="fa fa-magic" aria-hidden="true"></i>

                            </div>
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

            </div>
        </div>
</div>
</div>

<!-- js for portfolio lightbox -->
<script src="js/jquery.chocolat.js "></script>
<link rel="stylesheet " href="css/chocolat.css " type="text/css" media="all" />
<!--light-box-files -->
<script type="text/javascript ">
    $(function () {
        $('.portfolio-grids a').Chocolat();
    });
</script>
<!-- /js for portfolio lightbox -->

<?php include 'footer.php'; ?>
