
<%
Response.Redirect "http://www.lord-erroll.com"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<TITLE>The Lord Erroll - Gourmet Restaurant - Verandah, Claremont, Conservatory, Highlander Bar</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="Classification" CONTENT="Restaurants">
<META NAME="Description" CONTENT="The Lord Erroll is one of the finest restaurants in East Africa and is situated in one of the most pleasant residential areas of Nairobi and 15 minutes away from the city.">
<META NAME="KeyWords" CONTENT="restaurants in Nairobi,restaurants in Kenya,international cuisine in nairobi,eating out in Nairobi,going out in Nairobi,restaurants in East Africa,fine dining in Nairobi,Nairobi restaurants,wedding venues in Nairobi,seminar facilities in Nairobi,up-market restaurants in East Africa,French cuisine in Nairobi,romantic bars in Nairobi,exclusive restaurants in Nairobi">
<link href="le.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="swiss-restaurant/icon.ico">
<style type="text/css">
<!--
.style4 {color: #006600; font-weight: bold; }
-->
</style>
</head>

<body>
<table width="900" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td align="center"><a href="http://www.lord-erroll.com/index.asp"><img src="swiss-restaurant/le-logo.gif" alt="Lord Erroll - Gourmet Restaurant" width="315" height="70" border="0" /></a><a href="index.php"></a><a href="index.asp"></a></td>
  </tr>
  <tr>
    <td class="iBdr"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="iWht">
      <tr>
        <td class="iMenus"><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="11%"><a href="http://www.lord-erroll.com/index.asp">Home</a></td>
            <td width="12%"><a title="The Lord Erroll - Gourmet Restaurant" href="http://www.lord-erroll.com/restaurant.asp">Restaurants</a></td>
            <td width="9%"><a title="A selection of menus from The Lord Erroll" href="http://www.lord-erroll.com/menus.asp">Our Menu</a></td>
            <td width="16%"><a title="Ideal venue for corporate and private functions" href="http://www.lord-erroll.com/functions.asp">Corporate Functions</a></td>
            <td width="14%"><a title="The Lord Erroll features special days with special themes" href="http://www.lord-erroll.com/specials.asp">Special Days </a></td>
            <td width="12%"><a title="Photo gallery from Lord Erroll" href="http://www.lord-erroll.com/photo-gallery.asp">Photo Gallery</a></td>
            <td width="13%"><a title="Lord Erroll - Training" href="http://www.lord-erroll.com/training.asp">Training</a></td>
            <td width="13%"><a title="The Lord Erroll reservations and contact information" href="http://www.lord-erroll.com/contacts.asp">Reservations</a></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td><div align="center"></div></td>
      </tr>
      <tr>
        <td class="mSec"><table width="100%" border="0" cellspacing="0" cellpadding="10">
          <tr>
            <td width="43%" rowspan="5" align="center" valign="middle"><table width="90%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <th scope="col"><div align="center"><img src="swiss-restaurant/decor-top.gif" alt="Sep" width="316" height="28" /></div></th>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/bar_flags.jpg" alt="Verandah" name="" width="360" height="209" class="picbdr" /></td>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/decor-bot.gif" alt="Sep" width="316" height="28" /></td>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/claremont_rest.jpg" alt="Claremont" width="360" height="208" class="picbdr" /></td>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/decor-bot.gif" alt="Sep" width="316" height="28" /></td>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/conservatory_rest.jpg" alt="Conservatory" width="360" height="209" class="picbdr" /></td>
              </tr>
              <tr>
                <td><div align="center">
                  <p><img src="swiss-restaurant/decor-bot.gif" alt="Sep" width="316" height="28" /></p>
                </div></td>
              </tr>

              <tr>
                <td><img src="swiss-restaurant/veranda_rest.jpg" alt="Conservatory" width="360" height="209" class="picbdr" /></td>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/decor-bot.gif" alt="Sep" width="316" height="28" /></td>
              </tr>
              <tr>
                <td><img src="swiss-restaurant/bar_rest.jpg" alt="Highlander" width="360" height="209" class="picbdr" /></td>
              </tr>
              <tr>
                <td><div align="center"><img src="swiss-restaurant/decor-bot.gif" alt="Sep" width="316" height="28" /></div></td>
              </tr>
            </table></td>
            <td width="57%" height="321" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="header"><img src="swiss-restaurant/restaurants.gif" width="319" height="32" /></td>
              </tr>
              <tr>
                <td class="spacer5"> <p> The Lord Erroll is made up of multiple dining rooms that can be booked privately for groups as small as six and as large as eighty. Over the years, The Lord Erroll Restaurant has attracted a very loyal local following as well as an international clientele.<br />
</p>
                  <p align="justify">With exquisite Classic French cuisine, superior service and a winning wine list, The Lord Erroll is the premier French and gourmet restaurant in East Africa. </p>
                  <p align="justify"> Lose yourself in the romantic atmosphere as you relax by our  fireplaces. Savor the fine food and sip fine wine for a memorable evening. Our experienced staff will pamper you. </p>
                  <p>&nbsp;</p></td>
              </tr>
              <tr>
                <td align="center"><img src="swiss-restaurant/sep.gif" width="316" height="12" />                  <table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr align="center" valign="top">
                      <td>&nbsp;</td>
                      </tr>
                    </table></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td width="57%" height="269" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="header"><img src="swiss-restaurant/claremon.gif" width="341" height="32" /></td>
              </tr>
              <tr>
                <td class="spacer5"><p>The Claremont is 100 square meters and boasts a lovely fireplace. It consists of rich mahogany panelled interior reminiscent of a traditional Victorian &quot;Club Room&quot;. It takes upto 80 people at any given time. Its carpeted floors make it the ideal venue for seminars, national days of different countries, art exhibiproduct launches and even private parties and dinners.  <br />
                  <br />
                The Claremont's doors all open out to the vast Veranda and garden - both can be hired and used in conjunction.</p>                    </td>
              </tr>
              <tr>
                <td align="center"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                  <tr align="center" valign="top">
                    <td>&nbsp;</td>
                        </tr>
                </table>
                  <br />
                  <br />
                  <br />
                  <img src="swiss-restaurant/sep.gif" width="316" height="12" /></td>
              </tr>

            </table></td>
          </tr>
          <tr>
            <td width="57%" height="525" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="header"><a name="Conservatory" id="Conservatory"></a><img src="swiss-restaurant/conser.gif" width="364" height="32" /></td>
              </tr>
              <tr>
                <td class="spacer5"><p> The concept of the Conservatory is expressed perfectly in the colours and décor of the room. The mahogany floor, gentle light and illustrations of birds, plants and butterflies enhance the mood The room is 100 square meters and takes upto 80 people at any given time.<br />
                  <br />
                Like the Claremont it boasts a large fireplace and also opens out to the vast Veranda and garden. Depending on preference, it can also be used for seminars, product launches and private parties or dinners</p></td>
              </tr>
              <tr>
                <td height="103" align="center"><br />
                    <br />
                        <br />
                      <img src="swiss-restaurant/sep.gif" width="316" height="12" /></td>
              </tr>
              <tr>
                <td class="spacer5"><span class="header"><img src="swiss-restaurant/verand.gif" alt="" width="333" height="32" /></span></td>
              </tr>
              <tr>
                <td class="spacer5">Our renowned terrace which adjoins the Highlander Bar, the Conservatory and the Claremont overlooks our delightful garden setting, complete with waterfalls, streams and ponds. This lovely terrace is ideal for &quot;<em><strong>al fresco</strong></em>&quot; dining (<em>lunch or dinner</em>), morning coffee or afternoon tea and is also available for private functions. It can take upto 150 people, giving everyone a chance to sit outside on a beautiful day. <br />
                  <br />
                  It has become the ultimate dining venue with more people opting to sit and enjoy the outdoor dining. For cool evenings, it is equipped with patio heaters which keep everyone comfortable during the meal while overlooking the well lit gardens.</td>
              </tr>

            </table>
              <div align="center"><br />
                  <br />
                  <img src="swiss-restaurant/sep.gif" alt="" width="316" height="12" /></div></td>
          </tr>
          <tr>
            <td width="57%" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="header"><a name="Highlander" id="Highlander"></a><img src="swiss-restaurant/highlan.gif" alt="Highlander Bar" width="346" height="32" /></td>
              </tr>
              <tr>
                <td class="spacer5"><p>The tartan carpet and elegantly furnished lounge with a fireplace make The Highlander a traditional but elegant bar, providing customers with a perfect setting for pre-lunch and dinner aperitifs or just a quiet drink with friends. </p>
                    <p>The wood panelling, brass and vintage photographic memorabilia set the scene of the Erroll (Happy Valley) era of Kenya in the late 1930's. The room is 80 square metres and can be used for small meetings or seminars. </p></td>
              </tr>

              <tr>
                <td align="center"><img src="swiss-restaurant/sep.gif" width="316" height="12" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="1" cellpadding="4" cellspacing="0" bordercolor="#8F8047">
              <tr bgcolor="#FFFFFF">
                <td width="36%"><span class="style4">Service </span></td>
                <td width="30%"><span class="style4">Open </span></td>
                <td width="34%"><span class="style4">Last order </span></td>
              </tr>
              <tr bgcolor="#FFFFFF">
                <td>Dinner </td>
                <td><strong>6:00pm</strong></td>
                <td><strong> 10:30pm</strong></td>
              </tr>
              <tr bgcolor="#FFFFFF">
                <td>Lunch </td>
                <td><strong>12:00pm</strong></td>
                <td><strong> 5:00pm</strong></td>
              </tr>
              <tr bgcolor="#FFFFFF">
                <td>Spouse Night </td>
                <td><strong>Every Thursday</strong></td>
                 </tr>
                 <tr bgcolor="#FFFFFF">
                <td>Monday </td>
                <td><strong>Restaurant Closed </strong></td>
                 </tr>
            </table>
              <br /> <p align="center"><a href="contacts.asp" class="encased"><strong>Book your reservations now </strong></a></p></td>
          </tr>

        </table></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td align="center" class="footer"><p class="btmLinks"><a href="http://www.lord-erroll.com/index.asp">Home</a> | <a href="http://www.lord-erroll.com/restaurant.asp">Restaurants</a> | <a title="A selection of menus from The Lord Erroll" href="http://www.lord-erroll.com/menus.asp">Menus</a> | <a title="Ideal venue for corporate and private functions" href="http://www.lord-erroll.com/functions.asp">Corporate Functions</a><a title="Ideal venue for corporate functions" href="http://www.lord-erroll.com/corporate-functions.asp"></a> | <a title="The Lord Erroll features special days with special themes" href="http://www.lord-erroll.com/specials.asp">Special Days</a> |<a href="http://www.lord-erroll.com/giftvouchers.asp"> Gift Vouchers</a> | <a title="Picture and photos from Lord Erroll" href="http://www.lord-erroll.com/photo-gallery.asp">Photo Gallery</a> | <a title="Lord Erroll - Training" href="http://www.lord-erroll.com/training.asp">Training</a> | <a title="The Lord Erroll reservations and contact information" href="http://www.lord-erroll.com/contacts.asp">Reservations</a></p>
    <p>Copyright © 2008 - <strong>The Lord Erroll - Gourmet         Restaurant</strong> - All Rights Reserved</p></td>
  </tr>
</table>
</body>
</html>
