$(document).ready(function () {

    $("#rsubmit").click(function (e) {
       e.preventDefault();
       rname = $("#rname").val();
       email = $("#remail").val();
       phone = $("#rphone").val();
       rorg = $("#rorg").val();
       rdate = $("#datepicker").val();
       rtime = $('#country').find(":selected").text();
       rno = $("#rno").val();
       other = $('textarea#other').val();
       rtime_2 = $("#r_time").val();

       //alert(rtime_2);

       $.ajax({
           url:'/reserve',
           method:'post',
           data:{
               name:rname,
               email:email,
               phone:phone,
               org:rorg,
               date:rdate,
               time:rtime,
               no:rno,
               other:other,
               rtime_2:rtime_2
           },
           success:function () {
               $("#rname").val('');
               $("#remail").val('');
               $("#rphone").val('');
               $("#rorg").val('');
               $("#rno").val('');
               $("#r_time").val('');
               $('textarea#other').val('');
               alert('Reservation for '+rname+' has been sent!')
           }
       })


    });

    $("#isubmit").click(function (e) {
        e.preventDefault();
        iname = $("#iname").val();
        email = $("#iemail").val();
        message = $('textarea#imessage').val();

        $.ajax({
           url:"/inquiry",
            method:'post',
            data:{
               name:iname,
                email:email,
                message:message
            },
            success:function () {
                $("#iname").val('');
                $("#iemail").val('');
                $('textarea#imessage').val('');
                alert('Your inquiry has been sent!');
            }
        });
    });

    $("#menushow").click(function (e) {
        e.preventDefault();
       $("#menu").delay(500).fadeIn();
       $("#closemenu").attr("style","display:block");
        $("#openmenu").attr("style","display:none");
    });

    $("#menuclose").click(function (e) {
        e.preventDefault();
        $("#menu").delay(100).fadeOut();
        $("#closemenu").attr("style","display:none");
        $("#openmenu").attr("style","display:block");
    });

    $('[href="#profile"]').on('shown.bs.tab', function (e) {
        console.log("Trying to resize profile");
        console.log($('#mslider2').width());
        $('.menu-slide2').slick('setPosition', 0);
        $('#mslider2').resize();
        console.log($('#mslider2').width());
        console.log("Done");
    });

    $('[href="#home"]').on('shown.bs.tab', function (e) {
        console.log("Trying to resize home");
        console.log($('#mslider').width());
        $('.menu-slide').slick('setPosition', 0);
        $('#mslider').resize();
        console.log($('#mslider').width());
        console.log("Done");
    });

    $('[href="#test"]').on('shown.bs.tab', function (e) {
        console.log("Trying to resize home");
        console.log($('#mslider3').width());
        $('.menu-slide3').slick('setPosition', 0);
        $('#mslider3').resize();
        console.log($('#mslider3').width());
        console.log("Done");
    });
    $('[href="#wines"]').on('shown.bs.tab', function (e) {
        console.log("Trying to resize home");
        console.log($('#mslider4').width());
        $('.menu-slide4').slick('setPosition', 0);
        $('#mslider3').resize();
        console.log($('#mslider4').width());
        console.log("Done");
    });

    $('.customer-logos').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });


    $('.menu-slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        dots: true,
        fade: true,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $('.menu-slide2').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 1000,
        arrows: true,
        dots: true,
        fade: true,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $('.menu-slide3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 1000,
        arrows: true,
        dots: true,
        fade: true,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    $('.menu-slide4').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 1000,
        arrows: true,
        dots: true,
        fade: true,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});

function openNav() {
    document.getElementById("mb").style.display = "none";
    document.getElementById("myNav").style.width = "100%";
}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("mb").style.display = "block";
    document.getElementById("myNav").style.width = "0%";
}
