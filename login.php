<?php
ini_set("display_errors", 0);
ini_set("error_reporting", 0);

include 'connection.php';

if(empty($_SESSION['member'])){
include 'header.php';
include 'menu.php';
?>
<div class="container">
  <div class="col-md-12" style="border-top: 1px solid #b4b4b4;padding-top:3em">
    <h1 class="tittle text-center" style="color:#dfac18">Administrator Login</h1>
  </div>
  <div class="col-md-6 col-md-offset-3" style="padding-top:5em;padding-bottom:5em">
    <?php if(isset($_GET['error'])){?>
              <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error ! </strong>Incorrect Username and/or Password
              </div>
              <?php } ?>
    <form action="authenticate.php" method="post">
        <div class="phone_email" style="width:100%">
            <label><span class="fa fa-user" aria-hidden="true"></span> Username : </label>
            <div class="form-text">
                <input class="form-control" style="width:100%" type="text" name="username" placeholder="Username" required="">
            </div>
        </div>
        <div class="phone_email phone_email1" style="width:100%">
            <label><span class="fa fa-lock" aria-hidden="true"></span> Password : </label>
            <div class="form-text">
                <input class="form-control" style="width:100% !important" type="password" name="password" placeholder="Password" required="">
            </div>
        </div>
        <div class="clearfix"></div>
        <input class="my-button" style="width:100%" type="submit" value="LOGIN"/>
    </form>
  </div>
</div>
<?php include 'footer.php';}else{ header("location:admin/index.php"); } ?>
