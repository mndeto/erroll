<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'le_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Abc1234#');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x*o}`U8!]a(?E5MsE@T}SKVZ9{dkeY*HV:1L-!#P0b+uhME0gi^(I)Jx33/5:}.y');
define('SECURE_AUTH_KEY',  'DJ0:L;`v/AYZSa4<{Rhm^W5&M1U)fzmhm)VEEJgg4#dryOl/!Mh+fa}p2]p}htE_');
define('LOGGED_IN_KEY',    '}Hy=>4hCP({7UP$X*W62(0fLK{m-Yk_iqn;1)Q1SQ[A/Rm)X1S`Mh;?Z;V~Uk7s$');
define('NONCE_KEY',        '  $<%WMh3Ph]RSc*MfCe8$;Ui3?MrU%TJ4nygOyaZ2ZDDCQ*XVPB~zAoiiI@!TcW');
define('AUTH_SALT',        'H3)!GXi}*X5I93xWd^}~nLjjw`<3ga Xvc5W|@?+bGn~w$5bCMHv|vt7%uJ^Xzi.');
define('SECURE_AUTH_SALT', 'pZ- DkLD6?WV91aHh:JSy;El_F2-NmKFxmMG[Ty,fmj?<~lK8-7B|l:,|rq~:n0m');
define('LOGGED_IN_SALT',   '$JE11^13#_}j?TEc;#QGo(`@8:bGPeJGItB8_VokS.McFJ%0tDc/8#tA !kZ@ QC');
define('NONCE_SALT',       '.pk-@+qj^n4yOcRq}sA3NwL6OQJ`ikL*3YYY5FG{sC{Cv,ry5EtC}o&rl!u9OnTE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FORCE_SSL_ADMIN', true);
//define('COOKIE_DOMAIN','');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

