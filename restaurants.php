<?php
include 'header.php';
include 'menu.php';
?>
<div class="container" style="border-top: 1px solid #b4b4b4;padding-top:3em">
  <h3 class="tittle">OUR RESTAURANTS</h3>
  <div class="gallery_gds">
    <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
      <a href="claremont-room.php" title="The Claremont">
        <div class="agileits-img">
                <img class="img-responsive img-style erroll-img" src="images/claremont_rest.jpg" alt=""/>
        </div><br>
        <h4 class="erroll-link2 text-center" style="font-size:25px">The Claremont</h4>
        </a>
    </div>
    <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
      <a href="conservatory-room.php" title="The Conservatory">
        <div class="agileits-img">
                <img class="img-responsive img-style erroll-img" src="images/conservatory_rest.jpg" alt=""/>
        </div><br>
        <h4 class="erroll-link2 text-center" style="font-size:25px">The Conservatory</h4>
        </a>
    </div>
    <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
      <a href="highlander-lounge.php" title="The Highlander Lounge / Terraces">
        <div class="agileits-img">
                <img class="img-responsive img-style erroll-img" src="images/bar_rest.jpg" alt=""/>
        </div><br>
        <h4 class="erroll-link2 text-center" style="font-size:25px">The Highlander Lounge/Terrace</h4>
        </a>
    </div>
      <div class="col-md-12"></div>
      <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
        <a href="lady-idina.php" title="The Lady Idina Terraces">
          <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/lady_idina.jpg" alt=""/>
          </div><br>
          <h4 class="erroll-link2 text-center" style="font-size:25px">The Lady Idina Terraces</h4>
          </a>
      </div>
      <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
        <a href="diana-hay.php" title="The Diana Hay Garden">
          <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/diana_hay.jpg" alt=""/>
          </div><br>
          <h4 class="erroll-link2 text-center" style="font-size:25px">The Diana Hay Garden</h4>
          </a>
      </div>
      <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
        <a href="josslyn-hay.php" title="The Josslyn Hay Lounge">
          <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/josslyn_hay.jpg" alt=""/>
          </div><br>
          <h4 class="erroll-link2 text-center" style="font-size:25px">The Josslyn Hay Executive Lounge</h4>
          </a>
      </div>
      <div class="col-md-12" style="margin-top:5em">

      </div>


      <div class="clearfix"></div>
  </div>
</div>

<?php include 'footer.php'; ?>
