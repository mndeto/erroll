<?php
include 'header.php';
include 'menu.php';
ini_set( "display_errors", 0 );
ini_set( "error_reporting", 0 );


$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://www.lord-erroll.com/blog/wp-json/wp/v2/posts",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_POSTFIELDS => "",
	CURLOPT_HTTPHEADER => array(
		"Postman-Token: d58b5d2e-4ce1-47e6-a356-c05322cbd312",
		"cache-control: no-cache"
	),
));



$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
?>
<!-- Button trigger modal -->
<!--<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>-->

<!-- Modal -->

<!-- banner -->
<div class="banner">
    <div id="slideshow">
        <script>
            $("#slideshow > div:gt(0)").hide();

            setInterval(function() {
                $('#slideshow > div:first')
                // .fadeOut(2000)
                    .next()
                    .fadeIn(2000)
                    .end()
                    .appendTo('#slideshow');
            },  6000);
        </script>
		<?php

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$res = json_decode($response, true);
		foreach ($res as $item) {

		foreach ($item['categories'] as $cat) {
			if ( $cat == 72 ) {
				$curl2 = curl_init();

				curl_setopt_array( $curl2, array(
					CURLOPT_URL            => "https://www.lord-erroll.com/blog/wp-json/wp/v2/media/" . $item['featured_media'],
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING       => "",
					CURLOPT_MAXREDIRS      => 10,
					CURLOPT_TIMEOUT        => 30,
					CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST  => "GET",
					CURLOPT_HTTPHEADER     => array(
						"cache-control: no-cache",
						"postman-token: 3fb64d1f-65d4-ab33-6dfc-5d66c5115149"
					),
				) );

				$response2 = curl_exec( $curl2 );
				$err2      = curl_error( $curl2 );

				curl_close( $curl2 );

				$res2 = json_decode( $response2, true );

			?>
            <div>
                <a href="<?php echo $item['link']; ?>" target="_blank"><img style="width:100%;" src="<?php echo $res2['guid']['rendered']; ?>"></a>
            </div>
			<?php
		}}}}
        ?>

        <!--<div>
            <img style="width:100%" src="images/ban2.jpg">
        </div>
        <div>
            <img style="width:100%" src="images/ban3.jpg">
        </div>-->
    </div>
</div>
<!-- //banner-section -->
<div class="container-fluid"  id="about2" style="text-align: center">
    <div class="col-md-12 col-sm-12 col-xs-12 ab-w3l-text" >

<!--        <a href="https://www.luxuryrestaurantawards.com/node/add/vote/218856" target="_blank">-->
<!--            <img src="images/VotingButton.png" style="width:25%">-->
<!--        </a>-->
<!---->
<!--        <p style="font-size:18px;line-height:25px;text-align:center;font-weight: bold">-->
<!--            We have been nominated for The Luxury Restaurant Award. Please vote for us by clicking on the button above. Your support means everything to us. Thank you.-->
<!--        </p>-->
        <a href="https://www.hautegrandeur.com/restaurants/restaurant/lord-erroll-gourmet-restaurant/" target="_blank">
            <img src="images/awards_new.jpg" style="width:100%">
        </a>
    </div>
</div>
<!--/about -->
<div class="about" id="about" style="margin-top:0em !important">
    <div class="container">
        <div class="wthree-about" style="padding-top:2em">

            <div class="col-md-6 ab-w3l-text">
                <h2><span class="gold">ABOUT US</span></h2>


                <p class="goldlink">
                    <a style="text-decoration: none;font-size:17px;" target="_blank" href="https://www.hautegrandeur.com/restaurants/restaurant/lord-erroll-gourmet-restaurant/">
                        Haute Grandeur Global Restaurant Awards 2020 Shortlisted.
                    </a>
                </p>
								<p class="goldlink">
                    <a style="text-decoration: none;font-size:17px;" target="_blank" href="docs/LTG The Lord Erroll Article.pdf">
                        Global Award Winner - Restaurant of the Year 2017 (LTG).
                    </a>
                </p>
                <p class="goldlink">
                    <a style="text-decoration: none;font-size:17px;" target="_blank"
                       href="http://edition.cnn.com/travel/article/hottest-new-global-restaurant-openings-2018/index.html">
                        CNN Travel - 14 hot new global restaurants for 2018.
                    </a>
                </p>
                <p class="goldlink">
                    <a style="text-decoration: none;font-size:17px;" target="_blank"
                       href="http://www.luxuryrestaurantawards.com/winners/2018-restaurant-awards">
                        World Luxury Restaurant Awards Winner 2018. (Continental Awards)
                    </a>
                </p>


                <p style="font-size:15px;line-height:25px;text-align:justify"> The premier French and gourmet restaurant in East Africa, The Lord Erroll offers diners a delightful array of alfresco seating areas to choose from, each overlooking a different façade of the enchanting green gardens complete with beatifully ornate waterfalls, streams and ponds.<br><br> The restaurant is made up of multiple dining rooms that can be booked privately for groups as small as six and as large as 100, for banqueting, conference, weddings, meetings and bespoke events, and has a lush garden that takes up to 300pax comfortably.
                    <br><br> A great venue for breakfast meetings, business lunches, serene dinners, or to simply indulge in afternoon high tea, or a quiet and discreet drink or two with clients, friends, family and loved ones. Over the years, The Lord Erroll has attracted a very loyal local following as  well as international clientele.</p><br>
                <!--<p style="text-align:center !important"><a href="about.php" class="my-button">Read More </a></p>--><br>
            </div>
            <div class="col-md-6 about-w3l-agileifo-grid" >
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    <img src="images/salmon.jpg" alt=" " class="img-responsive">

                    <div class="img-ab"><img src="images/logoglass.jpg" alt=" " class="img-responsive"><br>

                        <!-- <h2>Global Awards 2017<br>

							 <h3 style="margin-bottom: 9em;">Restaurant of the Year</h3></h2>-->
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>

        </div>

    </div>

</div>

<!--Code to retrieve Google ratings-->

<?php

$curl = curl_init();

curl_setopt_array( $curl, array(
	CURLOPT_URL            => "https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJYeSBWdcXLxgRMhLAaVuue2w&key=AIzaSyDb5mXQZCdgc2_sjDcoW4X7qZfqcir_jb8",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING       => "",
	CURLOPT_MAXREDIRS      => 10,
	CURLOPT_TIMEOUT        => 30,
	CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST  => "GET",
	CURLOPT_SSL_VERIFYPEER => false,
	CURLOPT_HTTPHEADER     => array(
		"cache-control: no-cache",
		"postman-token: b724d23b-77bb-0822-3535-df7e9e170e6d"
	),
) );

$response = curl_exec( $curl );
$err      = curl_error( $curl );

curl_close( $curl );

if ( $err ) {
	echo "cURL Error #:" . $err;
} else {
	$res = json_decode( $response, true );
}
?>

<script>
    console.log(<?php echo $response; ?>);
    /* Open when someone clicks on the span element */
</script>


<!--Fin-->
<div class="container" style="background-color: #f5f5f5;margin-bottom: 2em;">
    <div class="gal">
        <div style="padding:1em" class="col-md-4 col-xs-6"><a href="images/1.jpg" data-toggle="lightbox"><img src="images/1.jpg" alt=""></a></div>
        <div style="padding:1em" class="col-md-4 col-xs-6"><a href="images/4.jpg" data-toggle="lightbox"><img src="images/4.jpg" alt=""></a></div>
        <div style="padding:1em" class="col-md-4 col-xs-6"><a href="images/7.jpg" data-toggle="lightbox"><img src="images/7.jpg" alt=""></a></div>
        <div style="padding:1em" class="col-md-4 col-xs-6"><a href="images/2.jpg" data-toggle="lightbox"><img src="images/2.jpg" alt=""></a></div>
        <div style="padding:1em" class="col-md-4 col-xs-6"><a href="images/9.jpg" data-toggle="lightbox"><img src="images/9.jpg" alt=""></a></div>
        <div style="padding:1em" class="col-md-4 col-xs-6"><a href="images/11.jpg" data-toggle="lightbox"><img src="images/11.jpg" alt=""></a></div>
    </div>
</div>
<div class="about awards" id="about" style="margin-top:0em;background: #f6f6f6;padding-bottom: 0em !important;">
    <div class="container">

        <div class="col-md-12 ab-w3l-text">
            <br><br>
            <h2 class="text-center"><span style="color: #dfac18">AWARDS AND ACCOLADES</span></h2><br>
            <div class="col-md-12">
                <div class="customer-logos" style="max-width:100%">
                    <div class="">
                        <img style="max-width:100%" src="images/wlra2018.png"/>
                    </div>
                    <div class="">
                        <img style="max-width:100%" src="images/ta2018.png"/>
                    </div>
                    <div class="">
                        <img style="max-width:100%" src="images/cnntravel.png"/>
                    </div>
                    <div class="">
                        <img style="max-width:100%" src="images/ga.png"/>
                    </div>
                    <div class="">
                        <img style="max-width:100%" src="images/lorderroll-01.png"/>
                    </div>
                    <div class="">
                        <img style="max-width:100%" src="images/tacoe.png"/>
                    </div>
                    <div class="">
                        <div class="text-center grate"
                             style="font-size: 20px;background: #f5f5f5;font-weight: bold;">
                            <p style="text-align: center"><img style="max-width:100%" src="images/google_logo.png"/></p>
                            <p style="text-align: center;color: #dfac18 !important;margin-top:-2em"><?php echo $res["result"]["rating"]; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="height:2em"></div>

        <style>
            #CDSWIDEXC.widEXC .widEXCLINK {
                padding: 13px 9px 0 9px !important;
            }
            .widEXC{
                width: 100% !important;
                min-height: 120px !important;
                margin: 0px !important;
            }
            #CDSSCROLLINGRAVE.narrow {
                width: 100% !important;
                min-height: 84px !important;

            }
            .TA_cdsscrollingravenarrow{
                /* padding-top: 8px !important; */
            }
            #CDSWIDREST .wrapper {
                width: 100% !important;
                min-height: 84px !important;
            }
            .wrapper{
                background-image: url(https://static.tacdn.com/img2/widget/restaurantWidget/restaurantWidget-green-knifeAndFork_v2.png);
                background-size: 100% 84px !important;
            }
            #CDSWIDREST {
                display: block !important;
            }
            #CDSWIDREST .wrapperLink .rightSide .rightWrapper {
                padding-left: 3.5em !important;
                padding-top: .9em !important;
            }
            marquee{
                padding-top: .8em !important;
            }
            .widEXCIMG{
                padding-top: 1em !important
            }
            #CDSWIDWRM.widWRMWrapper {
                margin: 0;
                padding: 0;
                width: 100% !important;
                min-height: 120px !important;
                border: none;
                background-color: #00a680;
                background-image: none;
                overflow: hidden;
                height: auto;
                position: relative;
            }
            .erroll-img2 {
                transform: scale(1.0);
                transition: all 0.5s;
            }
            .erroll-img2:hover {
                transform: scale(1.1);
                transition: all 0.5s;
            }

        </style>

        <div class="col-md-4 col-md-offset-2 erroll-img2" style="margin-bottom:3em;">
            <div id="TA_excellent395" class="TA_excellent">
                <ul id="Vt0Q1ZsrP4" class="TA_links 9qqHJt7">
                    <li id="Tx03P0dQw19" class="kbVOKm6M7m">
                        <a target="_blank" href="https://www.tripadvisor.com/"><img src="https://static.tacdn.com/img2/widget/tripadvisor_logo_115x18.gif" alt="TripAdvisor" class="widEXCIMG" id="CDSWIDEXCLOGO"/></a>
                    </li>
                </ul>
            </div>
            <script async src="https://www.jscache.com/wejs?wtype=excellent&amp;uniq=395&amp;locationId=2589490&amp;lang=en_US&amp;display_version=2"></script>
        </div>
        <div class="col-md-4 erroll-img2" style="margin-bottom:3em;">
            <div id="TA_cdswritereviewnew187" class="TA_cdswritereviewnew">
                <ul id="cxPFWUrutty" class="TA_links 7KOCxku">
                    <li id="J3TOyO" class="v3xuUT">
                        <a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/medium-logo-29834-2.png" alt="TripAdvisor"/></a>
                    </li>
                </ul>
            </div>
            <script async src="https://www.jscache.com/wejs?wtype=cdswritereviewnew&amp;uniq=187&amp;locationId=2589490&amp;lang=en_US&amp;lang=en_US&amp;display_version=2"></script>
        </div>
    </div>
</div>

<!--//about -->
<!--<div class="about" id="about" style="margin-top:0em;background:#f5f5f5;">
    <div class="container">

        <div class="col-md-12">
            <h2 class="text-center"><span class="gold" style="font-size:1.5em">GOOGLE CLIENT REVIEWS</span></h2><br><br>
            <div class="col-md-8 col-md-offset-2 col-xs-12" style="margin-top:4em;margin-bottom:4em">

                <div id="testimonial-slider" class="owl-carousel">
                    <?php /*$reviews = $res["result"]["reviews"];
                    for ($i = 0; $i < 5; $i++) { */ ?>
                        <div class="testimonial">
                            <div class="pic">
                                <img src="<?php /*echo $reviews["$i"]["profile_photo_url"]; */ ?>" alt=""/>
                            </div>
                            <h3 class="testimonial-title" style="color:#dfac18">
                                <?php /*echo $reviews["$i"]["author_name"]; */ ?>
                                <small> (<i class="fa fa-star"></i> Rating : <?php /*echo $reviews["$i"]["rating"]; */ ?>)
                                </small>
                            </h3>
                            <p class="description">
                                <?php /*echo $reviews["$i"]["text"]; */ ?>
                            </p>
                        </div>
                    <?php /*} */ ?>
                </div>

            </div>
        </div>

    </div>
</div>
-->
<div class="container-fluid">
    <h2 class="text-center tittle ser" style="color: #dfac18;font-size: 3em;margin-top: 2em;">ARTICLES</h2>
    <div class="col-md-12 text-center" style="margin-bottom:4em;margin-top:4em;">
        <div class="col-md-2 col-md-offset-1" style="margin-bottom:3em;">
            <a target="_blank"
               href="https://www.standardmedia.co.ke/article/2001311878/new-coffee-man-brews-signature-drink-to-the-top">
                <img class="img-responsive img-style erroll-img" style="border:1px solid #000" src="images/coffeeman.png" width="100%"/><br><br>
                <h4 class="erroll-link2 text-center">New coffee man brews signature drink to the top</h4>
            </a>
        </div>
        <div class="col-md-2" style="margin-bottom:3em;">
            <a target="_blank"
               href="https://www.standardmedia.co.ke/business/article/2001289195/the-lord-erroll-bags-nine-world-luxury-restaurant-awards">
                <img class="img-responsive img-style erroll-img" style="border:1px solid #000" src="images/wlra_award.jpg" width="100%"/><br><br>
                <h4 class="erroll-link2 text-center">The Lord Erroll ranked among Africa’s best destination after winning nine World Luxury Restaurant Awards</h4>
            </a>
        </div>
        <div class="col-md-2" style="margin-bottom:3em;">
            <a target="_blank"
               href="https://www.standardmedia.co.ke/business/article/2001259761/lord-erroll-global-award-winner-luxury-travel-guide">
                <img class="img-responsive img-style erroll-img" style="border:1px solid #000" src="images/ltg.jpg" width="100%"/><br><br>
                <h4 class="erroll-link2 text-center">Lord Erroll receives restaurant of the year 2017 Global Award</h4>
            </a>
        </div>
        <div class="col-md-2" style="margin-bottom:3em;">
            <a target="_blank"
               href="http://edition.cnn.com/travel/article/hottest-new-global-restaurant-openings-2018/index.html">
                <img class="img-responsive img-style erroll-img" style="border:1px solid #000"  src="images/cnn.travel.jpg" width="100%"/><br><br>
                <h4 class="erroll-link2 text-center">CNN Travel - 14 hot new global restaurants for 2018.</h4>
            </a>
        </div>
        <div class="col-md-2" style="margin-bottom:3em;">
            <a target="_blank"
               href="https://www.tripadvisor.com/Restaurant_Review-g294207-d2589490-Reviews-The_Lord_Erroll-Nairobi.html">
                <img class="img-responsive img-style erroll-img" style="border:1px solid #000" src="images/ta20182.png" width="100%"/><br><br>
                <h4 class="erroll-link2 text-center">Trip Advisor</h4>
            </a>
        </div>

    </div>
</div>
<!-- /Services -->
<div class="agile-services-w3" id="services" style="background-color:#dfac18 !important">
    <div class="container">
        <h3 class="tittle ser">OUR MENU</h3>

        <div class="wthree-agile-sevre-grids col-md-12 col-xs-12" style="display:none">
            <div id="openmenu" class="col-md-6 wthree-agile-grid">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="" id="menushow" class="hi-icon icon1"></a>
                </div>
                <h4>Menu</h4>
            </div>
            <div id="closemenu" style="display: none" class="col-md-6 wthree-agile-grid">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="" id="menuclose" class="hi-icon icon1"></a>
                </div>
                <h4>Menu</h4>
            </div>
            <div class="col-md-6 wthree-agile-grid">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="#book" class="scroll hi-icon icon2"></a>
                </div>
                <h4>Reservation</h4>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="wthree-agile-sevre-grids" id="menu" style="display: block">
            <!--<div class="col-md-3 col-xs-3 wthree-agile-grid" style="margin-top: 2em;">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="" data-toggle="modal" data-target="#breakfast"><i class="white fa fa-coffee fa-5x"></i>
                    </a>
                </div>
                <h4>Breakfast</h4>
            </div>
            <div class="col-md-3 col-xs-3 wthree-agile-grid" style="margin-top: 2em;">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="" class="" data-toggle="modal" data-target="#alacarte"><i
                                class="white glyphicon glyphicon-cutlery fa-5x"></i></a>
                </div>
                <h4>A La Carte</h4>
            </div>
            <div class="col-md-3 col-xs-3 wthree-agile-grid" style="margin-top: 2em;">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="#" data-toggle="modal" data-target="#hightea"><i class="white fa fa-coffee fa-5x"></i></a>
                </div>
                <h4>High Tea</h4>
            </div>
            <div class="col-md-3 col-xs-3 wthree-agile-grid" style="margin-top: 2em;">
                <div class="hi-icon-wrap hi-icon-effect-7 hi-icon-effect-7b">
                    <a href="#" data-toggle="modal" data-target="#wines"><i class="white glyphicon glyphicon-glass fa-5x"></i></a>
                </div>
                <h4>Wines</h4>
            </div>-->
            <div class="col-md-8 col-md-offset-2 text-center">
                <iframe id="my_menu" frameborder='0'  style="width:100%;min-height:21em"  title='Lord Erroll Menu' src="https://www.lord-erroll.com/menu/" type='text/html' allowfullscreen='true' scrolling='no' marginwidth='0' marginheight='0'></iframe><br><br>
                <a style="text-decoration:none" href="https://www.lord-erroll.com/menu/" target="_blank"><button class="erroll-btn">View Menu in Full Screen</button></a>
                <style>
                    .erroll-btn{
                        padding: 1em 2em;
                        border: 2px solid #000;
                        color: #000;
                        background: #dfac18;
                        transition: all 0.5s;
                        font-size: 18px;
                        font-weight: 600;
                    }
                    .erroll-btn:hover{
                        border: 2px solid:#dfac18;
                        color: #fff;
                        background: #000;
                    }
                </style>
            </div>
            <div class="col-md-12 col-xs-12" style="margin-top:2em;color: #fff;font-weight: bold;text-align: center">Open everyday</div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //Services -->
<!-- gallery -->
<div id="gallery" class="gallery">
    <div class="container-fluid">
        <h3 class="tittle">OUR ROOMS</h3>
        <div class="gallery_gds col-md-12">
            <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
                <a href="claremont-room" title="The Claremont">
                    <div class="agileits-img">
                        <img class="img-responsive img-style erroll-img" src="images/claremont_rest.jpg" alt=""/>
                    </div><br>
                    <h4 class="erroll-link2 text-center" style="font-size:25px">The Claremont</h4>
                </a>
            </div>
            <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
                <a href="conservatory-room" title="The Conservatory">
                    <div class="agileits-img">
                        <img class="img-responsive img-style erroll-img" src="images/conservatory_rest.jpg" alt=""/>
                    </div><br>
                    <h4 class="erroll-link2 text-center" style="font-size:25px">The Conservatory</h4>
                </a>
            </div>
            <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
                <a href="highlander-lounge" title="The Highlander Lounge / Terraces">
                    <div class="agileits-img">
                        <img class="img-responsive img-style erroll-img" src="images/bar_rest.jpg" alt=""/>
                    </div><br>
                    <h4 class="erroll-link2 text-center" style="font-size:25px">The Highlander Lounge</h4>
                </a>
            </div>
            <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
                <a href="lady-idina" title="Lady Idina Terraces">
                    <div class="agileits-img">
                        <img class="img-responsive img-style erroll-img" src="images/lady_idina.jpg" alt=""/>
                    </div><br>
                    <h4 class="erroll-link2 text-center" style="font-size:25px">Lady <p></p> Idina Terraces</h4>
                </a>
            </div>
            <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
                <a href="diana-hay" title="The Diana Hay Garden">
                    <div class="agileits-img">
                        <img class="img-responsive img-style erroll-img" src="images/diana_hay.jpg" alt=""/>
                    </div><br>
                    <h4 class="erroll-link2 text-center" style="font-size:25px">The Diana Hay Garden</h4>
                </a>
            </div>
            <div class="col-md-4 gal-w3l" style="margin-bottom:2em">
                <a href="josslyn-hay" title="The Josslyn Hay Lounge">
                    <div class="agileits-img">
                        <img class="img-responsive img-style erroll-img" src="images/josslyn_hay.jpg" alt=""/>
                    </div><br>
                    <h4 class="erroll-link2 text-center" style="font-size:25px">The Josslyn Hay Executive Lounge</h4>
                </a>
            </div>
            <div class="col-md-12" style="margin-top:5em">

            </div>


            <div class="clearfix"></div>
        </div>
    </div>
    <br>
</div>
<!-- //gallery -->
<!-- reservation -->
<div class="reservation-w3laits" id="book">
    <div class="container">
        <h3 class="tittle">BOOK YOUR TABLE</h3>
        <!--        <h3 class="tittle" style="font-size: 95%;color:red;margin-bottom: 10px;">NOTICE: VALENTINES DAY BOOKINGS HAVE BEEN SOLD OUT. PLEASE MAKE RESERVATIONS FROM 15TH FEBRUARY ONLY</h3>-->

        <div class="agile-reservation">
            <div class="col-md-4 agile-reservation-grid">
                <img src="images/side.jpg"  alt=" " class="img-responsive img-style">
            </div>
            <div class="col-md-4 agile-reservation-grid mid-w3l-aits">
                <div class="book-form">
                    <form action="#" method="post">
                        <div class="phone_email">
                            <label>Full Name : </label>

                            <div class="form-text">
                                <span class="fa fa-user" aria-hidden="true"></span>
                                <input type="text" name="name" id="rname" placeholder="Name" required="">
                            </div>
                        </div>
                        <div class="phone_email phone_email1">
                            <label>Email : </label>

                            <div class="form-text">
                                <span class="fa fa-envelope" aria-hidden="true"></span>
                                <input type="email" name="email" id="remail" placeholder="Email" required="">
                            </div>
                        </div>
                        <div class="phone_email">
                            <label>Phone Number : </label>

                            <div class="form-text">
                                <span class="fa fa-phone" aria-hidden="true"></span>
                                <input type="text" name="phone" id="rphone" placeholder="Phone no" required="">
                            </div>
                        </div>
                        <div class="phone_email phone_email1">
                            <label>Organization : </label>

                            <div class="form-text">
                                <span class="fa fa-briefcase" aria-hidden="true"></span>
                                <input type="text" name="organization" id="rorg" placeholder="Organization" required="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="agileits_reservation_grid">
                            <div class="phone_email">
                                <label>Date : </label>

                                <div class="book_date">
                                    <span class="fa fa-calendar" aria-hidden="true"></span>
                                    <input class="date" id="datepicker" type="text" name="date" placeholder="mm/dd/yyyy"
                                           required="">
                                </div>
                            </div>
                            <div class="phone_email phone_email1">
                                <label>No.of People : </label>
                                <!-- start_section_room -->
                                <div class="section_room">
                                    <span class="fa fa-users" aria-hidden="true"></span>
                                    <input type="number" id="rno" name="no_of_people" placeholder="No. of People" min="1"
                                           required="">
                                </div>
                            </div>
                            <div class="span1_of_1">
                                <!-- start_section_room -->
                                <label>Choose Meal : </label>

                                <div class="section_room">
                                    <span class="fa fa-clock-o" aria-hidden="true"></span>
                                    <select id="country" onchange="change_country(this.value)"
                                            class="frm-field required">
                                        <option value="Breakfast">Break Fast <span style="float: right">&#9;&#9;</span></option>
                                        <option value="Lunch">Lunch <span style="float: right">&#9;&#9;</span></option>
                                        <option value="High Tea">High Tea <span style="float: right">&#9;&#9;</span></option>
                                        <option value="Dinner">Dinner <span style="float: right">&#9;&#9;</span></option>
                                        <option value="Highlander Lounge">Highlander Lounge<span style="float: right">&#9;&#9;</span></option>
                                    </select>
                                </div>
                            </div>
                            <div class="full">
                                <label>Time : </label>
                                <!-- start_section_room -->
                                <div class="section_room">
                                    <span class="fa fa-clock-o" aria-hidden="true"></span>
                                    <input type="text" id="r_time" name="reservation_time" placeholder="Type in your time" min="1"
                                           required="">
                                </div>
                            </div>
                            <div class="span1_of_1" style="margin-top: 1em">
                                <label>Other Information : </label>
                                <!-- start_section_room -->
                                <div class="section_room">
                                    <span class="fa fa-info" aria-hidden="true"></span>
                                    <textarea style="width: 100%;height: 4em" id="other" name="other"  min="1"
                                              required=""></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input id="rsubmit" type="submit" value="Book Now"/>
                    </form>
                </div>

            </div>
            <div class="col-md-4 agile-reservation-grid">
                <img src="images/hightea.jpg" alt=" " class="img-responsive img-style">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //reservation -->
<?php include 'footer.php'; ?>
