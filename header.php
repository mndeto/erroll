<!DOCTYPE html>
<html lang="en">
<head>
<title>The Lord Erroll - Gourmet Restaurant</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="The Lord Erroll - Gourmet Restaurant, best , restaurant, fine dining, date night, ladies night, gourmet" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="css/swipebox.css">
<link rel="stylesheet" href="css/testimonial.css">
<link rel="stylesheet" href="css/slideshow.css">
<link rel="stylesheet" href="css/ekko-lightbox.css">

<link rel="stylesheet" href="css/loader.css">

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/04f1e9aa77ed8d1c47806d78a/a678566290cd6fb1b417a4a5a.js");</script>

<!-- //Custom Theme files -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- font-awesome icons -->
<!-- //font-awesome icons -->
<link href="css/slickslider.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="http://kenwheeler.github.io/slick/slick/slick-theme.css"/>
    <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script> -->

<script src="https://use.fontawesome.com/61725da21d.js"></script>


<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href="//fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
<!-- //web-fonts -->
<script src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script src="js/slider.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/slider.css" />
<script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css"/>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css"/>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css">

<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/ekko-lightbox.js"></script>
<script>
$(document).ready(function(){
   $("#testimonial-slider").owlCarousel({
       items:1,
       itemsDesktop:[1000,1],
       itemsDesktopSmall:[979,1],
       itemsTablet:[768,1],
       pagination:true,
       navigation:false,
       navigationText:["",""],
       slideSpeed:1000,
       singleItem:true,
       transitionStyle:"fade",
       autoPlay:true
   });
});

</script>
    <!-- Global site tag (gtag.js) - Google Ads: 749506452 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-749506452"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-749506452');
    </script>


</head>
<body>
<div class="se-pre-con"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="js/carousel.js"></script>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=290288021461316&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


  <!--Sticky Social-->
  <aside style="z-index:100;" id="sticky-social">
      <ul class="my_ul">
          <li><a href="https://www.facebook.com/TheLordErroll/" class="my_link entypo-facebook" target="_blank"><span>Facebook</span></a></li>
          <li><a href="https://www.instagram.com/lorderroll/" class="my_link entypo-instagrem" target="_blank"><span>Instagram</span></a></li>
      </ul>
  </aside>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109847469-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109847469-2');
</script>

  <style>
  @import url(http://weloveiconfonts.com/api/?family=entypo);

  /* entypo */
  [class*="entypo-"]:before {
     font-family: "entypo", sans-serif;
  }
  .my_link {
     text-decoration: none;
  }
  .my_ul {
     list-style: none;
     margin: 0;
     padding: 0;
  }
  .my_container {
     margin: 0 auto;
     padding: 20px 50px;
     background: white;
  }
  #sticky-social {
     right: 0;
     position: fixed;
     top: 300px;
  }
  #sticky-social a {
     background: #333;
     color: #fff;
     display: block;
     height: 35px;
     font: 16px "Open Sans", sans-serif;
     line-height: 35px;
     position: relative;
     text-align: center;
     width: 35px;
     transition: all 0.5s;
  }
  #sticky-social a span {
     line-height: 35px;
     right: -120px;
     position: absolute;
     text-align:center;
     width:120px;
     transition: all 0.5s;
  }
  #sticky-social a:hover span {
     right: 100%;
     transition: all 0.5s;
  }
  #sticky-social a[class*="facebook"],
  #sticky-social a[class*="facebook"]:hover,
  #sticky-social a[class*="facebook"] span { background: #dfac18;transition: all 0.5s;}

  #sticky-social a[class*="twitter"],
  #sticky-social a[class*="twitter"]:hover,
  #sticky-social a[class*="twitter"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="gplus"],
  #sticky-social a[class*="gplus"]:hover,
  #sticky-social a[class*="gplus"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="linkedin"],
  #sticky-social a[class*="linkedin"]:hover,
  #sticky-social a[class*="linkedin"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="instagrem"],
  #sticky-social a[class*="instagrem"]:hover,
  #sticky-social a[class*="instagrem"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="stumbleupon"],
  #sticky-social a[class*="stumbleupon"]:hover,
  #sticky-social a[class*="stumbleupon"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="pinterest"],
  #sticky-social a[class*="pinterest"]:hover,
  #sticky-social a[class*="pinterest"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="flickr"],
  #sticky-social a[class*="flickr"]:hover,
  #sticky-social a[class*="flickr"] span { background: #dfac18;transition: all 0.5s; }

  #sticky-social a[class*="tumblr"],
  #sticky-social a[class*="tumblr"]:hover,
  #sticky-social a[class*="tumblr"] span { background: #dfac18;transition: all 0.5s; }
  </style>

  <!--sticky Social-->
