<!-- contact -->
<div class="contact contact-right2" style="background-color: #212121;color: #fff !important;padding: 5em 2em 5em 2em;min-height: auto" id="contact">
    <div class="col-md-12">
        <h4 class="text-center" style="color: #fff">GET IN TOUCH</h4>
    </div>
    <form action="#" method="post">
    <div class="col-md-3">
        <input style="width:100%" type="text" id="iname" name="Name" placeholder="Name" required="">
    </div>
        <div class="col-md-3">
            <input style="width:100%" class="email" id="iemail" type="email" name="Email" placeholder="Email" required="">
        </div>
        <div class="col-md-4">
            <textarea style="width:100%" placeholder="Message" id="imessage" name="Message" required=""></textarea>
        </div>
        <div class="col-md-2">
            <input style="width:100%" id="isubmit" type="submit" value="SUBMIT">
        </div>
    </form>
    <div class="clearfix"> </div>
</div>

	<div class="contact" id="contact">

		<div class="col-md-12 contact-right">
            <div class="wthree-contact-row" style="min-height: 500px"></div>
            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.911169249586!2d36.811918650949245!3d-1.2218252991044287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f17d75981e461%3A0x6c7bae5b69c01232!2sThe+Lord+Erroll+Gourmet+Restaurant!5e0!3m2!1sen!2ske!4v1518780227496" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
		</div>

		<div class="clearfix"> </div>
	</div>
	<!-- //contact -->
	 <!-- Footer -->
	<div class="w3l-footer">
		<div class="container">
         <div class="footer-info-agile">
		 <div class="col-md-4 footer-info-grid address">
					<h4>Contact <span class="opening">Us</span></h4>
					<address>
						<ul>


                            <li>The Lord Erroll Gourmet Restaurant</li>
                            <li>89 Ruaka Rd, Runda Estate,</li>
                            <li>P. O. Box 999-00621, Village Market</li>
                            <li>Nairobi, Kenya</li>
                            <li><br></li>
                            <li><i class="fa fa-phone"></i> +254-721-920820</li>
                            <li><a class="erroll-link" href="mailto:reservations@lord-erroll.com"><i class="fa fa-envelope"></i> reservations@lord-erroll.com</a></li>
						</ul>
					</address>
				</div>
				<div class="col-md-4 footer-info-grid opening_hours">
				<h4>Opening <span class="opening">Hours</span></h4>
						 <ul class="times">

                             <li><i class="glyphicon glyphicon-time"> </i><span class="week">Monday - Friday</span><div class="hours">7:00 am - 12:00 am</div>  <div class="clearfix"></div></li>
                             <br><p></p>
                             <li><i class="glyphicon glyphicon-time"> </i><span class="week">Saturday</span><div class="hours">8:00 am - 12:00 am</div>  <div class="clearfix"></div></li>
                             <br><p></p>
                             <li><i class="glyphicon glyphicon-time"> </i><span class="week">Sunday</span><div class="hours">9:00 am - 11:00 pm</div>  <div class="clearfix"></div></li>

						 </ul>
					</div>
				<div class="col-md-4 footer-grid">
				<h4>Our <span class="opening">Location</span></h4>
        <!-- Modal -->
        <div id="modal-map" style="width:auto" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="width:auto">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body text-center">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.911169249586!2d36.811918650949245!3d-1.2218252991044287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f17d75981e461%3A0x6c7bae5b69c01232!2sThe+Lord+Erroll+Gourmet+Restaurant!5e0!3m2!1sen!2ske!4v1518780227496" frameborder="0" style="width:100%;height:500px;border:0" allowfullscreen></iframe>
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div> -->
            </div>

          </div>
        </div>
        <!--/ Modal-->
					<a href="#" data-toggle="modal" data-target="#modal-map"><img src="images/map.jpg" style="width:100%"></a>
					<div class="clearfix"> </div>
				</div>

				<div class="clearfix"></div>
			</div>

			<div class="connect-agileits">
				<div class="col-md-12 connect-social">
					<h5 class="text-center">CONNECT WITH US</h5>
					<section class="social">
                        <ul>
							<li><a target="_blank" class="icon fb" href="https://www.facebook.com/TheLordErroll/"><i class="fa fa-facebook" style="font-size:35px;color:#fff"></i></a></li>
							<li><a target="_blank" class="icon gp" href="https://www.instagram.com/lorderroll/"><i class="fa fa-instagram" style="font-size:35px;color:#fff"></i></a></li>
						</ul>
					</section>

				</div>
				<!--<div class="col-md-6 newsletter">
					<h5>NEWSLETTER</h5>

					<form action="#" method="post" class="newsletter">
						<input class="email" type="email" placeholder="Your email address..." required="">

						<input type="submit" class="submit" value="">
					</form>
				</div>-->
				<div class="clearfix"></div>
			</div>
		</div>
        <div class="modal fade" id="alacarte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <!--<iframe src="menus/Lord%20Erroll%20New%20Ala%20Carte%20Menu%20.pdf"
									style="width:100%; height:100%;" frameborder="0"></iframe>-->
                            <object data="menus/Lord%20Erroll%20New%20Ala%20Carte%20Menu%20.pdf" type="application/pdf" width="100%" height="800px">
                                <p>It appears you don't have a PDF plugin for this browser.
                                    No biggie... you can <a href="menus/Lord%20Erroll%20New%20Ala%20Carte%20Menu%20.pdf">click here to
                                        download the PDF file.</a></p>
                            </object>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="breakfast" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <!--<iframe src="menus/Lord%20Erroll%20New%20Ala%20Carte%20Menu%20.pdf"
									style="width:100%; height:100%;" frameborder="0"></iframe>-->
                            <object data="menus/Lord%20Erroll%20breakfast%20Menu.pdf" type="application/pdf" width="100%" height="800px">
                                <p>It appears you don't have a PDF plugin for this browser.
                                    No biggie... you can <a href="menus/Lord%20Erroll%20breakfast%20Menu.pdf">click here to
                                        download the PDF file.</a></p>
                            </object>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="wines" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <!--<iframe src="menus/Lord%20Erroll%20New%20Ala%20Carte%20Menu%20.pdf"
									style="width:100%; height:100%;" frameborder="0"></iframe>-->
                            <object data="menus/Lord%20Erroll%20Wine%20List%20Menu.pdf" type="application/pdf" width="100%" height="800px">
                                <p>It appears you don't have a PDF plugin for this browser.
                                    No biggie... you can <a href="menus/Lord%20Erroll%20Wine%20List%20Menu.pdf">click here to
                                        download the PDF file.</a></p>
                            </object>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="hightea" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div style="text-align: center;">
                            <!--<iframe src="menus/Lord%20Erroll%20New%20Ala%20Carte%20Menu%20.pdf"
									style="width:100%; height:100%;" frameborder="0"></iframe>-->
                            <object data="menus/LE%20High%20Tea%20Menu.pdf" type="application/pdf" width="100%" height="800px">
                                <p>It appears you don't have a PDF plugin for this browser.
                                    No biggie... you can <a href="menus/LE%20High%20Tea%20Menu.pdf">click here to
                                        download the PDF file.</a></p>
                            </object>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
		<div class="copyright-wthree">
				<p>&copy; <?php date_default_timezone_set("Africa/Nairobi"); echo date("Y"); ?>  The Lord Eroll - Gourmet Restaurant . All Rights Reserved | Developed by <a href="http://www.zerone.co.ke/"> Zerone I.T Solutions </a></p>
			</div>
	</div>

<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

<!-- //smooth-scrolling-of-move-up -->
<!--banner-Slider-->
          <script src="js/responsiveslides.min.js"></script>
            <script>
              // You can also use "$(window).load(function() {"
              $(function () {
                // Slideshow 4
                $("#slider3").responsiveSlides({
                auto: true,
                pager:true,
                nav:false,
                speed:500,
                namespace: "callbacks",
                before: function () {
                  $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                  $('.events').append("<li>after event fired.</li>");
                }
                });

              });
             </script>
              <!-- Calendar -->

          <script>
              $(function() {
                $( "#datepicker" ).datepicker();
              });
          </script>
          <!-- //Calendar -->
<!--//banner-Slider-->
<!-- swipe box js -->
<script src="js/jquery.adipoli.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.row2').adipoli({
      'startEffect' : 'overlay',
      'hoverEffect' : 'sliceDown'
    });
  });

</script>
<script src="js/jquery.swipebox.min.js"></script>
<script type="text/javascript">
    jQuery(function($) {
      $(".swipebox").swipebox();
    });
</script>
<!-- //swipe box js -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
  $(document).ready(function() {
    /*
    var defaults = {
      containerID: 'toTop', // fading element id
      containerHoverID: 'toTopHover', // fading element hover id
      scrollSpeed: 1200,
      easingType: 'linear'
    };
    */

    $().UItoTop({ easingType: 'easeOutQuart' });

  });
</script>
<!-- Countdown-Timer-JavaScript -->
    <script src="js/simplyCountdown.js"></script>
    <script>
      var d = new Date(new Date().getTime() + 48 * 120 * 120 * 2000);

      // default example
      simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
      });

      // inline example
      simplyCountdown('.simply-countdown-inline', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        inline: true
      });

      //jQuery example
      $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
      });
    </script>
  <!-- //Countdown-Timer-JavaScript -->
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
      event.preventDefault();

  $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
  });
</script>
<!-- //end-smooth-scrolling -->
  <script src="js/bootstrap.js"></script>
  <script src="js/slideshow.js"></script>
<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>
<!-- <script type="text/javascript" src="js/loader.js"></script> -->
</body>
</html>
