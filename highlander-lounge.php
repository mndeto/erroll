<?php
include 'header.php';
include 'menu.php';
?>
<div class="container">
  <div class="col-md-12" style="border-top: 1px solid #b4b4b4;padding-top:3em">
    <h1 class="tittle" style="color:#dfac18">The Highlander Lounge</h1>
  </div>
  <div class="col-md-4" style="padding-top:5em;padding-bottom:5em;padding-right:2em">

    <p style="text-align:justify;line-height:37px;font-size:16px">The tartan carpet and elegantly furnished lounge with a fireplace make
The Highlander a traditional but elegant bar, providing customers with
a perfect setting for pre-lunch and dinner aperitifs or just a quiet drink
with friends.<br><br>The wood panelling, brass and vintage photographic memorabilia set
the scene of the Erroll (Happy Valley) era of Kenya in the late 1930's.
The room is 80 square metres and can be used for small meetings or
parties.</p>
  </div>
  <div class="col-md-8" style="margin-top:5em;margin-left:-25px !important;">
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:880px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:680px;overflow:hidden;">
            <div data-p="170.00">
                <img data-u="image" src="images/highland/1.jpg" />
                <img data-u="thumb" src="images/highland/1.jpg" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="images/highland/2.jpg" />
                <img data-u="thumb" src="images/highland/2.jpg" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="images/highland/3.jpg" />
                <img data-u="thumb" src="images/highland/3.jpg" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="images/highland/4.jpg" />
                <img data-u="thumb" src="images/highland/4.jpg" />
            </div>
            <div data-p="170.00">
                <img data-u="image" src="images/highland/5.jpg" />
                <img data-u="thumb" src="images/highland/5.jpg" />
            </div>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;bottom:0px;width:980px;height:150px;background-color:#fff;" data-autocenter="1" data-scale-bottom="0.75">
            <div data-u="slides">
                <div data-u="prototype" class="p" style="width:190px;height:90px;">
                    <div data-u="thumbnailtemplate" class="t"></div>
                    <svg viewbox="0 0 16000 16000" class="cv">
                        <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                        <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                        <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                    </svg>
                </div>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:300px;left:30px;" data-scale="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:300px;right:30px;" data-scale="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
            </svg>
        </div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>
  </div>
  <div class="gallery_gds col-md-12" style="margin-top:10em">
    <h2 style="margin-bottom:1em" class="text-center erroll-gold">More of Our Rooms</h2>
      <div class="col-md-2 gal-w3l" style="margin-bottom:2em">
          <a href="claremont-room.php" title="The Claremont">
              <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/claremont_rest.jpg" alt=""/>
              </div>
              <h4 class="erroll-link2 text-center">The Claremont</h4>
          </a>
      </div>
      <div class="col-md-2 gal-w3l" style="margin-bottom:2em">
          <a href="conservatory-room.php" title="The Conservatory">
              <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/conservatory_rest.jpg" alt=""/>
              </div>
              <h4 class="erroll-link2 text-center">The Conservatory</h4>
          </a>
      </div>
      <div class="col-md-2 gal-w3l" style="margin-bottom:2em">
          <a href="highlander-lounge.php" title="The Highlander Lounge / Terraces">
              <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/bar_rest.jpg" alt=""/>
              </div>
              <h4 class="erroll-link2 text-center">The Highlander Lounge</h4>
          </a>
      </div>
      <!-- <div class="col-md-12"></div> -->
      <div class="col-md-2 gal-w3l" style="margin-bottom:2em">
          <a href="lady-idina.php" title="The Lady Idina Terraces">
              <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/lady_idina.jpg" alt=""/>
              </div>
              <h4 class="erroll-link2 text-center">Lady Idina Terraces</h4>
          </a>
      </div>
      <div class="col-md-2 gal-w3l" style="margin-bottom:2em">
          <a href="diana-hay.php" title="The Diana Hay Garden">
              <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/diana_hay.jpg" alt=""/>
              </div>
              <h4 class="erroll-link2 text-center">The Diana Hay Garden</h4>
          </a>
      </div>
      <div class="col-md-2 gal-w3l" style="margin-bottom:2em">
          <a href="josslyn-hay.php" title="The Josslyn Hay Lounge">
              <div class="agileits-img">
                  <img class="img-responsive img-style erroll-img" src="images/josslyn_hay.jpg" alt=""/>
              </div>
              <h4 class="erroll-link2 text-center">The Josslyn Hay Executive Lounge</h4>
          </a>
      </div>
      <div class="col-md-12" style="margin-top:5em">

      </div>


      <div class="clearfix"></div>
  </div>
</div>

<?php include 'footer.php'; ?>
