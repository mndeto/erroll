<?php
include 'header.php';
include 'menu.php';
?>
<div class="container" style="border-top: 1px solid #b4b4b4;padding-top:3em">
  <h3 class="tittle">CONTACT US</h3><br><br><br>
  <div class="col-md-4 text-center" style="margin-bottom:4em">
    <i class="fa fa-envelope fa-5x erroll-gold"></i><br><br>
    <p style="font-size:16px"><a class="erroll-link2" href="mailto:reservations@lord-erroll.com">reservations@lord-erroll.com</a></p>
  </div>
  <div class="col-md-4 text-center" style="margin-bottom:4em">
    <i class="fa fa-map-marker fa-5x erroll-gold"></i><br><br>
    <p style="font-size:16px">
      The Lord Erroll Gourmet Restaurant<br>
      89 Ruaka Rd, Runda Estate,<br>
      P. O. Box 999-00621, Village Market<br>
      Nairobi, Kenya
    </p>
  </div>
  <div class="col-md-4 text-center" style="margin-bottom:4em">
    <i class="fa fa-phone fa-5x erroll-gold"></i><br><br>
    <p style="font-size:16px">+254-721-920820</p>
  </div>
  <div class="col-md-12" style="margin-top:4em">

  </div>
</div>
<?php include 'footer.php'; ?>
