<!--<div class="col-md-12" style="background: #3a3939;padding: 0.5em;color: white;">
    <div class="container">
    <span style="font-size:14px"><i class="fa fa-envelope" style="color:#8c6f0f"></i> <a class="erroll-link" href="mailto:reservations@lord-erroll.com">reservations@lord-erroll.com</a></span> <span style="float:right;font-size:14px">+254-721-920820 <i style="color:#8c6f0f" class="fa fa-phone gold"></i></span>
    </div>
</div>
<h1 class="text-center"><a href="index.php"><img src="images/logo.png" style="max-width: 100%"></a></h1>

  <div class="header">
    <div class="container">

      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button style="float:left" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div style="text-align:center !important">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

          <ul class="nav navbar-nav">
            <li><a href="index.php" class="hvr-underline-from-center">Home</a></li>
            <li><a href="restaurants.php" class="hvr-underline-from-center">Restaurants</a></li>
            <li><a href="#" class="hvr-underline-from-center">Our Menu</a></li>
            <li><a href="#" class="hvr-underline-from-center">Corporate Functions</a></li>
            <li><a href="#" class="hvr-underline-from-center">Special Days</a></li>
            <li><a href="#" class="hvr-underline-from-center">Photo Gallery</a></li>
            <li><a href="#" class="hvr-underline-from-center">Training</a></li>
            <li><a href="reservations.php" class="hvr-underline-from-center">Reservations</a></li>
            <li><a href="contact.php" class="hvr-underline-from-center">Contact Us</a></li>
          </ul>

        </div>
        </div>
        <div class="clearfix"> </div>
      </nav>
    </div>
  </div>-->

<!-- The overlay -->
<div class="col-md-6 col-md-offset-3 col-xs-12">
<div id="myNav" class="overlay">

    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <!-- Overlay content -->
    <div class="overlay-content">
        <a href="index">Home</a>
        <a href="rooms">Our Rooms</a>
        <a href="gallery">Our Gallery</a>
<a href="ourevents">Events</a>
<a target="_blank"  href="/menu">Menu</a>
<a target="_blank"  href="/blog">Blog</a>
        <a href="reservations">Reservations</a>
        <a href="contact">Contact</a>
    </div>

</div>
</div>
<!-- Use any element to open/show the overlay navigation menu -->
<span onclick="openNav()" class="menu-button1"> <img id="mb" src="images/menu.png" /> </span>
