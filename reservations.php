<?php
include 'header.php';
include 'menu.php';
?>
<!-- reservation -->
<div class="reservation-w3laits" id="book">
    <div class="container">
        <h3 class="tittle">BOOK YOUR TABLE</h3>

        <div class="agile-reservation">
            <div class="col-md-4 agile-reservation-grid">
                <img src="images/side.jpg" style="height: 100%" alt=" " class="img-responsive img-style">
            </div>
            <div class="col-md-4 agile-reservation-grid mid-w3l-aits">
                <div class="book-form">
                    <form action="#" method="post">
                        <div class="phone_email">
                            <label>Full Name : </label>

                            <div class="form-text">
                                <span class="fa fa-user" aria-hidden="true"></span>
                                <input type="text" name="name" id="rname" placeholder="Name" required="">
                            </div>
                        </div>
                        <div class="phone_email phone_email1">
                            <label>Email : </label>

                            <div class="form-text">
                                <span class="fa fa-envelope" aria-hidden="true"></span>
                                <input type="email" name="email" id="remail" placeholder="Email" required="">
                            </div>
                        </div>
                        <div class="phone_email">
                            <label>Phone Number : </label>

                            <div class="form-text">
                                <span class="fa fa-phone" aria-hidden="true"></span>
                                <input type="text" name="phone" id="rphone" placeholder="Phone no" required="">
                            </div>
                        </div>
                        <div class="phone_email phone_email1">
                            <label>Organization : </label>

                            <div class="form-text">
                                <span class="fa fa-briefcase" aria-hidden="true"></span>
                                <input type="text" name="organization" id="rorg" placeholder="Organization" required="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="agileits_reservation_grid">
                            <div class="phone_email">
                                <label>Date : </label>

                                <div class="book_date">
                                    <span class="fa fa-calendar" aria-hidden="true"></span>
                                    <input class="date" id="datepicker" type="text" name="date" placeholder="mm/dd/yyyy"
                                           required="">
                                </div>
                            </div>
                            <div class="phone_email phone_email1">
                                <label>No.of People : </label>
                                <!-- start_section_room -->
                                <div class="section_room">
                                    <span class="fa fa-users" aria-hidden="true"></span>
                                    <input type="number" id="rno" name="no_of_people" placeholder="No. of People" min="1"
                                           required="">
                                </div>
                            </div>
                            <div class="span1_of_1">
                                <!-- start_section_room -->
                                <label>Choose Meal : </label>

                                <div class="section_room">
                                    <span class="fa fa-clock-o" aria-hidden="true"></span>
                                    <select id="country" onchange="change_country(this.value)"
                                            class="frm-field required">
                                        <option value="Breakfast">Break Fast <span style="float: right">&#9;&#9;</span></option>
                                        <option value="Lunch">Lunch <span style="float: right">&#9;&#9;</span></option>
                                        <option value="High Tea">High Tea <span style="float: right">&#9;&#9;</span></option>
                                        <option value="Dinner">Dinner <span style="float: right">&#9;&#9;</span></option>
                                        <option value="Highlander Lounge">Highlander Lounge<span style="float: right">&#9;&#9;</span></option>
                                    </select>
                                </div>
                            </div>
                            <div class="full">
                                <label>Time : </label>
                                <!-- start_section_room -->
                                <div class="section_room">
                                    <span class="fa fa-clock-o" aria-hidden="true"></span>
                                    <input type="text" id="r_time" name="reservation_time" placeholder="Type in your time" min="1"
                                           required="">
                                </div>
                            </div>
                            <div class="span1_of_1" style="margin-top: 1em">
                                <label>Other Information : </label>
                                <!-- start_section_room -->
                                <div class="section_room">
                                    <span class="fa fa-info" aria-hidden="true"></span>
                                    <textarea style="width: 100%;height: 3em" id="other" name="other"  min="1"
                                              required=""></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input id="rsubmit" type="submit" value="Book Now"/>
                    </form>
                </div>

            </div>
            <div class="col-md-4 agile-reservation-grid">
                <img src="images/hightea.jpg" style="height:100%" alt=" " class="img-responsive img-style">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //reservation -->
<?php
include 'footer.php';
?>
